// index page scripts
$(document).ready(function() {
    // This is just an arbitrary script to
    // demonstrate changing template content from an
    // external JS script.
    var indexMsgDom = $('#index-message');
    indexMsgDom.text('Today is ' + Date());
});
