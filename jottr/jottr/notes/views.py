from datetime import datetime
from django.shortcuts import render

from jottr.notes.forms import NoteForm
from jottr.notes.models import Note


def index(request):
    # Based on our settings Django will look for this template file
    # in jottr/jottr/templates/notes
    template_name = 'notes/index.html'

    # The context dictionary allows us to pass variable data
    # to be used from within the template.
    context = {
        'title': 'Jottr',
        'greeting': 'Hello world!',
    }

    # Testing forms
    # We will display the last record on the note table and have
    # have the user edit it

    # This is a shortcut to get the last table record
    note = Note.objects.last()

    if not request.POST:  # If this is not a form submission

        # We initialize the form using the using the note object
        form = NoteForm(instance=note)

    else:

        # We initialize the form with the request POST data and
        # have it set to the note object
        form = NoteForm(request.POST, instance=note)

    # Validate and save the form if a post data is submitted
    if form.is_valid() and request.POST:
        form.save()

    # We then add the note form in the context var
    # so we can access it in the template
    context['note_form'] = form

    return render(request, template_name, context=context)
