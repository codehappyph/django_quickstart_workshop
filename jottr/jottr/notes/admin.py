from django.contrib import admin

from jottr.notes.models import Note


# This is as basic as it gets. You can however enhance the admin interface
# for this model with additional filters, search, layout by sub-classing
# the default admin.ModelAdmin for this model.
#
# Ref: https://docs.djangoproject.com/en/stable/ref/contrib/admin/
admin.site.register(Note)
