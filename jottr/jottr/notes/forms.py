from django.forms import ModelForm
from jottr.notes.models import Note


# Further reading on model forms:
# https://docs.djangoproject.com/en/forms/topics/forms/modelforms/#modelform
class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = [
            'summary',
            'detail',
        ]
