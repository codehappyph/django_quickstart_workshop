from django.db import models


class Note(models.Model):

    # Ref: https://docs.djangoproject.com/en/stable/ref/models/fields/#datetimefield
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    # Ref: https://docs.djangoproject.com/en/stable/ref/models/fields/#django.db.models.CharField
    summary = models.CharField(max_length=256)

    # Ref: https://docs.djangoproject.com/en/stable/ref/models/fields/#django.db.models.TextField
    detail = models.TextField()
